import React, {useState} from 'react';
import {View, FlatList, Text, Keyboard} from 'react-native';
import styles from './styles.js';

import {Formik, ErrorMessage} from 'formik';
import * as yup from 'yup';

import Card from '../../components/CardTemplate/index.js';
import TextInputForm from '../../components/TextInputForm';
import SubmitBtn from '../../components/SubmitBtn';

//definition of form scheema
const formScheema = yup.object({
  FirstName: yup.string().required().min(4),
  LastName: yup.string().required().min(5),
  Email: yup.string().required().email(),
  Balance: yup.number().required().positive(),
});

const ShowListScreen = ({navigation}) => {
  const [employerList, setEmployerList] = useState([]);

  let renderedCard = ({item}) => {
    function toNavigate() {
      navigation.navigate('Employer details', {
        first: item.FirstName,
        last: item.LastName,
        email: item.Email,
        balance: item.Balance,
      });
    }

    return (
      <Card
        firstName={item.FirstName}
        lastName={item.LastName}
        balance={item.Balance}
        navigation={toNavigate}
      />
    );
  };

  return (
    <Formik
      initialValues={{FirstName: '', LastName: '', Email: '', Balance: ''}}
      validationSchema={formScheema}
      onSubmit={(values, actions) => {
        setEmployerList(employerList => [...employerList, values]);
        Keyboard.dismiss();
        actions.resetForm();
      }}>
      {({handleBlur, handleChange, handleSubmit, values}) => (
        <View>
          <View style={styles.fromContainer}>
            <TextInputForm
              pHolder={'Enter your first name'}
              changeTxt={handleChange('FirstName')}
              blurField={handleBlur('FirstName')}
              val={values.FirstName}
            />
            <Text style={styles.errorTxt}>
              <ErrorMessage name="FirstName" />
            </Text>

            <TextInputForm
              pHolder={'Enter your last name'}
              changeTxt={handleChange('LastName')}
              blurField={handleBlur('LastName')}
              val={values.LastName}
            />
            <Text style={styles.errorTxt}>
              <ErrorMessage name="LastName" />
            </Text>

            <TextInputForm
              pHolder={'Enter your Email'}
              changeTxt={handleChange('Email')}
              blurField={handleBlur('Email')}
              val={values.Email}
            />
            <Text style={styles.errorTxt}>
              <ErrorMessage name="Email" />
            </Text>

            <TextInputForm
              pHolder={'Enter your balance'}
              changeTxt={handleChange('Balance')}
              blurField={handleBlur('Balance')}
              val={values.Balance}
              kbrdType="numeric"
            />
            <Text style={styles.errorTxt}>
              <ErrorMessage name="Balance" />
            </Text>

            <SubmitBtn handlePress={handleSubmit} txtBtn={'Submit'} />
          </View>

          <View style={styles.listContainer}>
            <FlatList data={employerList} renderItem={renderedCard} />
          </View>
        </View>
      )}
    </Formik>
  );
};

export default ShowListScreen;
