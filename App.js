import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Constantes from '../xtraineeApp/App/Utils/Constantes';

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="List Employers" component={Constantes.HOME} />
        <Stack.Screen name="Employer details" component={Constantes.DETAILS} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
