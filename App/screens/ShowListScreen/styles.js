import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  fromContainer: {borderColor: 'black', borderWidth: 1, padding: 10},
  listContainer: {
    borderTopColor: 'black',
    borderTopWidth: 1,
  },
  errorTxt: {
    color: 'red',
    alignSelf: 'center',
  },
});

export default styles;
