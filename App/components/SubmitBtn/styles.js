import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  btnContainer: {
    alignSelf: 'center',
    width: '40%',
    backgroundColor: '#4B84C8',
    borderRadius: 20,
    padding: 5,
    marginTop: 5,
  },
  txtBtn: {
    alignSelf: 'center',
    fontSize: 20,
    color: 'white',
  },
});

export default styles;
