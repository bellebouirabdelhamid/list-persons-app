import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import avatar from '../../../assets/Avatar.png';

import styles from './styles';

const Card = ({navigation, firstName, lastName, balance}) => {
  return (
    <TouchableOpacity style={styles.mainContainer} onPress={navigation}>
      <View style={styles.imageContainer}>
        <Image source={avatar} style={styles.img} />
      </View>
      <View style={styles.infoSConatainer}>
        <View>
          <Text style={styles.textStyle}>First name :</Text>
          <Text style={styles.textStyle}>Last name :</Text>
          <Text style={styles.textStyle}>Balance :</Text>
        </View>
        <View>
          <Text style={styles.textStyle}>{firstName}</Text>
          <Text style={styles.textStyle}>{lastName}</Text>
          <Text style={styles.textStyle}>{balance} $</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default Card;
