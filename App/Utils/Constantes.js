import ShowListScreen from '../screens/ShowListScreen';
import Details from '../screens/Details';

const ScreenNames = {HOME: ShowListScreen, DETAILS: Details};

export default ScreenNames;
