import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  fieldContainer: {
    width: '70%',
    margin: 5,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 20,
    alignSelf: 'center',
  },
  txtField: {
    alignSelf: 'center',
    fontSize: 17,
  },
});

export default styles;
