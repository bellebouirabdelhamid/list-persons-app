import React from 'react';
import {View, TextInput} from 'react-native';
import styles from './styles';

const index = ({pHolder, changeTxt, blurField, val, kbrdType}) => {
  return (
    <View style={styles.fieldContainer}>
      <TextInput
        style={styles.txtField}
        placeholder={pHolder}
        onChangeText={changeTxt}
        onBlur={blurField}
        value={val}
        keyboardType={kbrdType}
      />
    </View>
  );
};

export default index;
