import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import styles from './styles';

const index = ({handlePress, txtBtn}) => {
  return (
    <TouchableOpacity style={styles.btnContainer} onPress={handlePress}>
      <Text style={styles.txtBtn}>{txtBtn}</Text>
    </TouchableOpacity>
  );
};

export default index;
