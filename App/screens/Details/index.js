import React from 'react';
import {View, Text, Image} from 'react-native';
import styles from './styles';
import avatar from '../../../assets/Avatar.png';

const Index = ({route}) => {
  const {first, last, email, balance} = route.params;
  return (
    <View style={styles.mainContainer}>
      <View style={styles.imageContainer}>
        <Image source={avatar} style={styles.img} />
      </View>
      <View style={styles.infoSConatainer}>
        <View style={styles.textContainer}>
          <Text style={styles.textStyle}>First name :</Text>
          <Text style={styles.textStyle}>Last name :</Text>
          <Text style={styles.textStyle}>Email :</Text>
          <Text style={styles.textStyle}>Balance :</Text>
        </View>
        <View style={styles.txt}>
          <Text style={styles.textStyle}>{first}</Text>
          <Text style={styles.textStyle}>{last}</Text>
          <Text style={styles.textStyle}>{email}</Text>
          <Text style={styles.textStyle}>{balance} $</Text>
        </View>
      </View>
    </View>
  );
};

export default Index;
