import {StyleSheet, PixelRatio} from 'react-native';

const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'row',
    width: '90%',
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 20,
    alignSelf: 'center',
    marginVertical:5,
    backgroundColor:'#fbfbfb'
  },
  imageContainer: {
    width: '35%',
    alignItems: 'center',
  },
  img: {
    margin: 10,
    width: 90,
    height: 100,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor:'#bebebe'
  },
  infoSConatainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '60%',
    alignItems: 'center',
  },
  textStyle: {
    fontSize: 15,
    fontFamily: 'verdana',
    marginVertical: 5,
  },
});

export default styles;
