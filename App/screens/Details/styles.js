import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#fbfbfb',
    flex: 1,
  },
  imageContainer: {
    alignItems: 'center',
  },
  img: {
    margin: 10,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor: '#bebebe',
  },
  infoSConatainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  textStyle: {
    fontSize: 20,
    fontFamily: 'verdana',
    marginVertical: 5,
  },
  // textContainer: {
  //   width: '27%',
  // },
  // txt: {
  //   width: '60%',
  // },
});

export default styles;
